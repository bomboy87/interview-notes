# Helpful Links
- System Design Primer: https://github.com/donnemartin/system-design-primer
- How to rock a systems design interview: http://www.palantir.com/2011/10/how-to-rock-a-systems-design-interview/
- System Design Interviewing: http://www.hiredintech.com/system-design/
- Scalability for Dummies: http://www.lecloud.net/tagged/scalability
- Introduction to Architecting Systems for Scale: http://lethain.com/introduction-to-architecting-systems-for-scale/
- Scalable System Design Patterns: http://horicky.blogspot.com/2010/10/scalable-system-design-patterns.html
- Scalable Web Architecture and Distributed Systems: http://www.aosabook.org/en/distsys.html
- What is the best way to design a web site to be highly scalable? http://programmers.stackexchange.com/a/108679/62739
- How web works? https://github.com/vasanthk/how-web-works
